<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AppWindow</name>
    <message>
        <location filename="../../src/RaZor/Interface/AppWindow.cpp" line="336"/>
        <source>Importing </source>
        <translation>Importing </translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppWindow.cpp" line="346"/>
        <source>Finished importing</source>
        <translation>Finished importing</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="22"/>
        <source>Failed to find an entity named</source>
        <translation>Failed to find an entity named</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="55"/>
        <source> component(s) not displayed.</source>
        <translation> component(s) not displayed.</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="330"/>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="332"/>
        <source>Add component</source>
        <translation>Add component</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="336"/>
        <source>Transform</source>
        <translation>Transform</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="349"/>
        <source>Mesh</source>
        <translation>Mesh</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="362"/>
        <source>Light</source>
        <translation>Light</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="367"/>
        <source>Point light</source>
        <translation>Point light</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="374"/>
        <source>Directional light</source>
        <translation>Directional light</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="384"/>
        <source>Sound</source>
        <translation>Sound</translation>
    </message>
</context>
<context>
    <name>AudioSystemSettings</name>
    <message>
        <location filename="../../interface/AudioSystemSettings.ui" line="17"/>
        <source>Audio system settings</source>
        <translation>Audio system settings</translation>
    </message>
    <message>
        <location filename="../../interface/AudioSystemSettings.ui" line="33"/>
        <source>Audio device</source>
        <translation>Audio device</translation>
    </message>
</context>
<context>
    <name>CameraComp</name>
    <message>
        <location filename="../../interface/CameraComp.ui" line="14"/>
        <location filename="../../interface/CameraComp.ui" line="17"/>
        <source>Camera</source>
        <translation>Camera</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="23"/>
        <source>Field of view</source>
        <translation>Field of view</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="30"/>
        <source>Camera type</source>
        <translation>Camera type</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="37"/>
        <source>Projection type</source>
        <translation>Projection type</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="77"/>
        <source>Free fly</source>
        <translation>Free fly</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="82"/>
        <source>Look-at</source>
        <translation>Look-at</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="91"/>
        <source>Perspective</source>
        <translation>Perspective</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="96"/>
        <source>Orthographic</source>
        <translation>Orthographic</translation>
    </message>
</context>
<context>
    <name>LightComp</name>
    <message>
        <location filename="../../interface/LightComp.ui" line="14"/>
        <location filename="../../interface/LightComp.ui" line="17"/>
        <source>Light</source>
        <translation>Light</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="23"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="130"/>
        <source>Energy</source>
        <translation>Energy</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="160"/>
        <source>Color (RGB)</source>
        <translation>Color (RGB)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="29"/>
        <source>Ready</source>
        <translation>Ready</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="49"/>
        <source>Import a file</source>
        <translation>Import a file</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="49"/>
        <source>Mesh</source>
        <translation>Mesh</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="76"/>
        <source>Unexpected empty field</source>
        <translation>Unexpected empty field</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="31"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="39"/>
        <source>&amp;View</source>
        <translation>&amp;View</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="43"/>
        <source>&amp;Windows</source>
        <translation>&amp;Windows</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="148"/>
        <source>Add entity</source>
        <translation>Add entity</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="197"/>
        <source>Components</source>
        <translation>Components</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="101"/>
        <source>Entities</source>
        <translation>Entities</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="52"/>
        <source>&amp;Tools</source>
        <translation>&amp;Tools</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="56"/>
        <source>&amp;Systems</source>
        <translation>&amp;Systems</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="67"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="155"/>
        <source>Unselect</source>
        <translation>Unselect</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="227"/>
        <source>&amp;Open...</source>
        <translation>&amp;Open...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="230"/>
        <source>Open a file</source>
        <translation>Open a file</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="238"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="241"/>
        <source>Quit RaZor</source>
        <translation>Quit RaZor</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="249"/>
        <source>&amp;Entities</source>
        <translation>&amp;Entities</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="254"/>
        <source>&amp;Components</source>
        <translation>&amp;Components</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="259"/>
        <source>&amp;Render system settings...</source>
        <translation>&amp;Render system settings...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="267"/>
        <source>&amp;Options...</source>
        <translation>&amp;Options...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="275"/>
        <source>&amp;About</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="280"/>
        <source>&amp;Audio system settings...</source>
        <translation>&amp;Audio system settings...</translation>
    </message>
</context>
<context>
    <name>MeshComp</name>
    <message>
        <location filename="../../interface/MeshComp.ui" line="14"/>
        <location filename="../../interface/MeshComp.ui" line="17"/>
        <source>Mesh</source>
        <translation>Mesh</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="23"/>
        <source>Vertex count</source>
        <translation>Vertex count</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="37"/>
        <source>Triangle count</source>
        <translation>Triangle count</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="51"/>
        <source>Render mode</source>
        <translation>Render mode</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="59"/>
        <source>Triangles</source>
        <translation>Triangles</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="64"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="72"/>
        <source>Mesh file</source>
        <translation>Mesh file</translation>
    </message>
</context>
<context>
    <name>RenderSystemSettings</name>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="17"/>
        <source>Render system settings</source>
        <translation>Render system settings</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="33"/>
        <source>Cubemap</source>
        <translation>Cubemap</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="58"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="100"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="110"/>
        <source>Bottom</source>
        <translation>Bottom</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="136"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="188"/>
        <source>Front</source>
        <translation>Front</translation>
    </message>
</context>
<context>
    <name>SoundComp</name>
    <message>
        <location filename="../../interface/SoundComp.ui" line="14"/>
        <location filename="../../interface/SoundComp.ui" line="17"/>
        <source>Sound</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="23"/>
        <source>Repeat</source>
        <translation>Repeat</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="37"/>
        <source>Sound file</source>
        <translation>Sound file</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="47"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="56"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="63"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="70"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="79"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="99"/>
        <source>Pitch</source>
        <translation>Pitch</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="119"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="133"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
</context>
<context>
    <name>TransformComp</name>
    <message>
        <location filename="../../interface/TransformComp.ui" line="14"/>
        <location filename="../../interface/TransformComp.ui" line="17"/>
        <source>Transform</source>
        <translation>Transform</translation>
    </message>
    <message>
        <location filename="../../interface/TransformComp.ui" line="23"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../../interface/TransformComp.ui" line="148"/>
        <source>Scale</source>
        <translation>Scale</translation>
    </message>
</context>
</TS>
