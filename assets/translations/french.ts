<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AppWindow</name>
    <message>
        <location filename="../../src/RaZor/Interface/AppWindow.cpp" line="336"/>
        <source>Importing </source>
        <translation>Chargement de </translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppWindow.cpp" line="346"/>
        <source>Finished importing</source>
        <translation>Chargement terminé</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="22"/>
        <source>Failed to find an entity named</source>
        <translation>N&apos;a pas pu trouver une entité appelée</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="55"/>
        <source> component(s) not displayed.</source>
        <translation> composant(s) non affiché(s).</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="330"/>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="332"/>
        <source>Add component</source>
        <translation>Ajouter composant</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="336"/>
        <source>Transform</source>
        <translation>Transformation</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="349"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="362"/>
        <source>Light</source>
        <translation>Lumière</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="367"/>
        <source>Point light</source>
        <translation>Lumière positionnelle</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="374"/>
        <source>Directional light</source>
        <translation>Lumière directionnelle</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/AppComponents.cpp" line="384"/>
        <source>Sound</source>
        <translation>Son</translation>
    </message>
</context>
<context>
    <name>AudioSystemSettings</name>
    <message>
        <location filename="../../interface/AudioSystemSettings.ui" line="17"/>
        <source>Audio system settings</source>
        <translation>Paramètres du système audio</translation>
    </message>
    <message>
        <location filename="../../interface/AudioSystemSettings.ui" line="33"/>
        <source>Audio device</source>
        <translation>Périphérique audio</translation>
    </message>
</context>
<context>
    <name>CameraComp</name>
    <message>
        <location filename="../../interface/CameraComp.ui" line="14"/>
        <location filename="../../interface/CameraComp.ui" line="17"/>
        <source>Camera</source>
        <translation>Caméra</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="23"/>
        <source>Field of view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="30"/>
        <source>Camera type</source>
        <translation>Type de caméra</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="37"/>
        <source>Projection type</source>
        <translation>Type de projection</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="77"/>
        <source>Free fly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="82"/>
        <source>Look-at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="91"/>
        <source>Perspective</source>
        <translation>Perspective</translation>
    </message>
    <message>
        <location filename="../../interface/CameraComp.ui" line="96"/>
        <source>Orthographic</source>
        <translation>Orthographique</translation>
    </message>
</context>
<context>
    <name>LightComp</name>
    <message>
        <location filename="../../interface/LightComp.ui" line="14"/>
        <location filename="../../interface/LightComp.ui" line="17"/>
        <source>Light</source>
        <translation>Lumière</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="23"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="130"/>
        <source>Energy</source>
        <translation>Énergie</translation>
    </message>
    <message>
        <location filename="../../interface/LightComp.ui" line="160"/>
        <source>Color (RGB)</source>
        <translation>Couleur (RVB)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="29"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="49"/>
        <source>Import a file</source>
        <translation>Importer un fichier</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="49"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../../src/RaZor/Interface/MainWindow.cpp" line="76"/>
        <source>Unexpected empty field</source>
        <translation>Champ vide inattendu</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="31"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="39"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="43"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenêtres</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="148"/>
        <source>Add entity</source>
        <translation>Ajouter entité</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="197"/>
        <source>Components</source>
        <translation>Composants</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="101"/>
        <source>Entities</source>
        <translation>Entités</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="52"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="56"/>
        <source>&amp;Systems</source>
        <translation>&amp;Systèmes</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="67"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="155"/>
        <source>Unselect</source>
        <translation>Désélectionner</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="227"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="230"/>
        <source>Open a file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="238"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="241"/>
        <source>Quit RaZor</source>
        <translation>Quitter RaZor</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="249"/>
        <source>&amp;Entities</source>
        <translation>&amp;Entités</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="254"/>
        <source>&amp;Components</source>
        <translation>&amp;Composants</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="259"/>
        <source>&amp;Render system settings...</source>
        <translation>Paramètres du système de &amp;rendu...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="267"/>
        <source>&amp;Options...</source>
        <translation>&amp;Options...</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="275"/>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <location filename="../../interface/RaZor.ui" line="280"/>
        <source>&amp;Audio system settings...</source>
        <translation>Paramètres du système &amp;audio...</translation>
    </message>
</context>
<context>
    <name>MeshComp</name>
    <message>
        <location filename="../../interface/MeshComp.ui" line="14"/>
        <location filename="../../interface/MeshComp.ui" line="17"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="23"/>
        <source>Vertex count</source>
        <translation>Nombre de sommets</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="37"/>
        <source>Triangle count</source>
        <translation>Nombre de triangles</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="51"/>
        <source>Render mode</source>
        <translation>Mode de rendu</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="59"/>
        <source>Triangles</source>
        <translation>Triangles</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="64"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../../interface/MeshComp.ui" line="72"/>
        <source>Mesh file</source>
        <translation>Fichier de maillage</translation>
    </message>
</context>
<context>
    <name>RenderSystemSettings</name>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="17"/>
        <source>Render system settings</source>
        <translation>Paramètres du système de rendu</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="33"/>
        <source>Cubemap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="58"/>
        <source>Back</source>
        <translation>Derrière</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="100"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="110"/>
        <source>Bottom</source>
        <translation>Dessous</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="136"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../../interface/RenderSystemSettings.ui" line="188"/>
        <source>Front</source>
        <translation>Devant</translation>
    </message>
</context>
<context>
    <name>SoundComp</name>
    <message>
        <location filename="../../interface/SoundComp.ui" line="14"/>
        <location filename="../../interface/SoundComp.ui" line="17"/>
        <source>Sound</source>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="23"/>
        <source>Repeat</source>
        <translation>Répéter</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="37"/>
        <source>Sound file</source>
        <translation>Fichier de son</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="47"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="56"/>
        <source>Play</source>
        <translation>Jouer</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="63"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="70"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="79"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="99"/>
        <source>Pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="119"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../../interface/SoundComp.ui" line="133"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
</context>
<context>
    <name>TransformComp</name>
    <message>
        <location filename="../../interface/TransformComp.ui" line="14"/>
        <location filename="../../interface/TransformComp.ui" line="17"/>
        <source>Transform</source>
        <translation>Transformation</translation>
    </message>
    <message>
        <location filename="../../interface/TransformComp.ui" line="23"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../../interface/TransformComp.ui" line="148"/>
        <source>Scale</source>
        <translation>Échelle</translation>
    </message>
</context>
</TS>
